import React from 'react'

class DeleteFlightOfTourist extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            flightList: [],
            flightId: 0,
            touristId: this.props.match.params.id
        }
    }

    render() {
        return <div className="container-fluid">
            <select className="form-control" onChange={(event) => this.setState({flightId: event.target.value})}>
                {this.state.flightList.map((flight) => {
                    return <option
                        key={flight.id}
                        value={flight.id}>{flight.whereFrom}, {flight.whereTo}, {flight.flightDate}, {flight.arrivalDate}, {flight.seatCount} {flight.touristCount}, {flight.ticketPrice} </option>
                })}
            </select>
            <button className="btn btn-danger" onClick={() => {
                this.deleteTouristFlight();
            }}>
                delete flight!
            </button>
        </div>
    }

    componentDidMount() {
        this.flightsForTourist();
    }

    flightsForTourist() {
        fetch(`http://localhost:8080/tourists/${this.props.match.params.id}/flights`)
            .then(response => response.json())
            .then(data => {
                this.setState({flightList: data})
            })
    }

    deleteTouristFlight() {
        fetch(`http://localhost:8080/tourists/${this.props.match.params.id}/deleteFlight`, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({id: this.state.flightId})
        });
        this.flightsForTourist();
        alert("tourist flight deleted!")
    }

}

export default DeleteFlightOfTourist;