import React from 'react'
import {Link} from 'react-router-dom'
import {} from 'react-bootstrap'

class ListOfTourists extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            touristList: []
        }
    }

    render() {
        return <div className="container-fluid">
            {this.state.touristList.map((tourist) => {
                return <div
                    key={tourist.id}>{tourist.id}, {tourist.name}, {tourist.surname}, {tourist.sex}, {tourist.country}, {tourist.notes}, {tourist.birthDate}
                    <div><Link to={`/tourists/${tourist.id}`}>
                        <button className="btn btn-primary">Tourist Details</button>
                    </Link>
                    <Link to={`/tourists/${tourist.id}/addFlight`}>
                        <button className="btn btn-success">add flight to this user</button>
                    </Link>
                    <Link to={`/tourists/${tourist.id}/deleteFlight`}>
                        <button  className="btn btn-danger">
                            delete flight of this user
                        </button>
                    </Link>
                    </div>
                </div>
            })}
        </div>
    }

    componentDidMount() {
        this.findTourists();
    }

    findTourists() {
        fetch("http://localhost:8080/tourists")
            .then(response => response.json())
            .then(data => {
                this.setState({touristList: data, isLoading: false})
            })
    }
}

export default ListOfTourists;