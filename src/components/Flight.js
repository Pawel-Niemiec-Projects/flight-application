import React from 'react'
import './flight-application.css'

class Flight extends React.Component {
    constructor(props) {
        super(props);
        this.state = {flight: {}, isLoading: true, hasError: true}
    }

    render() {
        let flight = this.state.flight;
        if (!this.state.hasError && !this.state.isLoading) {
            return <div className="container-fluid"
                key={this.state.id}>{flight.id}, {flight.whereFrom}, {flight.whereTo}, {flight.flightDate}, {flight.arrivalDate}, {flight.seatCount} {flight.touristCount}, {flight.ticketPrice},{flight.tourists
                .map((tourist) => {
                    return <div
                        key={tourist.id}>{tourist.id}, {tourist.name}, {tourist.surname}, {tourist.sex}, {tourist.country}, {tourist.notes}, {tourist.birthDate} </div>
                })}
            </div>
        }
        return <div className="loader"/>
    }

    componentDidMount() {
        this.getFlight();
    }

    getFlight() {
        fetch(`http://localhost:8080/flights/${this.props.match.params.id}`)
            .then(response => response.json())
            .then(data => {
                this.setState({flight: data, isLoading: false, hasError: false})
            })
    }
}

export default Flight;