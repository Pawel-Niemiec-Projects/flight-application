import React from 'react'

class AddTouristFlight extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            flightList: [],
            touristId: this.props.match.params.id,
            flightId: 1
        };
    }

    render() {
        return <div className="container-fluid">
            <select className="form-control" onChange={(event) => {
                this.setState({flightId: event.target.value})
            }}>
                {this.state.flightList.map((flight) => {
                    return <option
                        key={flight.id}
                        value={flight.id}>{flight.whereFrom}, {flight.whereTo}, {flight.flightDate}, {flight.arrivalDate}, {flight.seatCount} {flight.touristCount}, {flight.ticketPrice} </option>
                })}
            </select>
            <button className="btn btn-primary" onClick={() => {
                this.addTouristFlight(JSON.stringify({flightId: this.state.flightId, touristId: this.state.touristId}))
            }}>
                add flight!
            </button>
        </div>
    }

    componentDidMount() {
        this.findFlights();
    }

    findFlights() {
        fetch("http://localhost:8080/flights")
            .then(response => response.json())
            .then(data => {
                this.setState({flightList: data});
            })
    }

    addTouristFlight(stringifiedRequest) {
        fetch('http://localhost:8080/flights/addTouristFlight', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: stringifiedRequest

        });
        alert("tourist flight added!")
    }
}

export default AddTouristFlight;