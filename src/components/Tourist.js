import React from 'react'

import './flight-application.css'

class Tourist extends React.Component {
    constructor(props) {
        super(props);
        this.state = {tourist: {}, isLoading: true, hasError: true,}
    }

    render() {
        let tourist = this.state.tourist;
        if (!this.state.hasError && !this.state.isLoading) {
            return <div>{tourist.id}, {tourist.name}, {tourist.surname}, {tourist.sex}, {tourist.country}, {tourist.notes}, {tourist.birthDate}, {tourist.flights.map((flight) => {
                return <div
                    key={flight.id}>{flight.id}, {flight.whereFrom}, {flight.whereTo}, {flight.flightDate}, {flight.arrivalDate}, {flight.seatCount} {flight.touristCount}, {flight.ticketPrice}</div>
            })}
            </div>
        }
        return <div className="loader"/>
    }

    componentDidMount() {
        this.getTourist();
    }

    getTourist() {
        fetch(`http://localhost:8080/tourists/${this.props.match.params.id}`)
            .then(response => response.json())
            .then(data => {
                this.setState({tourist: data, isLoading: false, hasError: false})
            })
    }
}

export default Tourist;