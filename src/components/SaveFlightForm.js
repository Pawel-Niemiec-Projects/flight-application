import React from 'react'
import {Button, Label} from 'react-bootstrap'

class SaveFlightForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            whereFrom: '',
            whereTo: '',
            flightDate: '',
            arrivalDate: '',
            seatCount: 0,
            touristCount: 0,
            ticketPrice:0
        }
    }

    render() {
        return <form className="form-group">
            <div>
                <Label htmlFor="whereTo"> whereTo</Label>
                <div className="col-10">
                    <input className="form-control" type="text" id="whereTo" onChange={(event) =>
                        this.setState({whereTo: event.target.value})
                    }/>
                </div>
            </div>
            <div>
                <Label htmlFor="whereFrom"> whereFrom</Label>
                <div className="col-10">
                    <input className="form-control" type="text" id="whereFrom" onChange={(event) =>
                        this.setState({whereFrom: event.target.value})
                    }/>
                </div>
            </div>
            <div>
                <Label htmlFor="flightDate"> flightDate</Label>
                <div className="col-10">
                    <input className="form-control" type="date" id="flightDate" onChange={(event) =>
                        this.setState({flightDate: event.target.value})
                    }/>
                </div>
            </div>
            <div>
                <Label htmlFor="arrivalDate"> arrivalDate</Label>
                <div className="col-10">
                    <input className="form-control" type="date" id="arrivalDate" onChange={(event) =>
                        this.setState({arrivalDate: event.target.value})
                    }/>
                </div>
            </div>
            <div>
                <Label htmlFor="seatCount"> seatCount</Label>
                <div className="col-10">
                    <input className="form-control" type="text" id="seatCount" onChange={(event) =>
                        this.setState({seatCount: event.target.value})
                    }/>
                </div>
            </div>
            <div>
                <Label htmlFor="touristCount"> touristCount</Label>
                <div className="col-10">
                    <input className="form-control" type="text" id="touristCount" onChange={(event) =>
                        this.setState({touristCount: event.target.value})
                    }/>
                </div>
            </div>
            <div>
                <Label htmlFor="ticketPrice"> ticketPrice</Label>
                <div className="col-10">
                    <input className="form-control" type="text" id="ticketPrice" onChange={(event) =>
                        this.setState({ticketPrice: event.target.value})
                    }/>
                </div>
            </div>
            <Button className="btn btn-primary"onClick={() => {
                this.saveNewFlight(JSON.stringify({
                    whereTo: this.state.whereTo,
                    whereFrom: this.state.whereFrom,
                    flightDate: this.state.flightDate,
                    arrivalDate: this.state.arrivalDate,
                    seatCount: this.state.seatCount,
                    touristCount: this.state.touristCount,
                    ticketPrice: this.state.ticketPrice
                }));
                alert("flight saved!")
            }}> Save flight!</Button>
        </form>
    }

    saveNewFlight(stringifiedUser) {
        fetch('http://localhost:8080/flights', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: stringifiedUser
        })
    }
}

export default SaveFlightForm;