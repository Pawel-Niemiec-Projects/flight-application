import React from 'react';
import {BrowserRouter as Router, Route,} from 'react-router-dom'
import ListOfTourists from './ListOfTourists'
import ListOfFlights from './ListOfFlights';
import Tourist from './Tourist'
import Flight from './Flight'
import SaveTouristForm from './SaveTouristForm'
import SaveFlightForm from './SaveFlightForm'
import AddTouristFlight from './AddTouristFlight'
import DeleteFlightOfTourist from './DeleteFlightOfTourist'

class FlightApplication extends React.Component {

    render() {
        return (
            <Router>
                <div className="container-fluid">
                    <ul className="nav justify-content-center">
                        <li className="nav-item">
                            <a className="nav-link active" href={'/tourists'}>
                                list of tourists
                            </a>
                        </li>
                        <li>
                            <a className="nav-link active" href={'/flights'}>
                                list of flights
                            </a>
                        </li>
                        <li>
                            <a className="nav-link active" href={'/save-tourist'}>
                                save new tourist
                            </a>
                        </li>
                        <li>
                            <a className="nav-link active" href={'/save-flight'}>
                                save new flight
                            </a>
                        </li>

                    </ul>
                    <Route exact={true} path='/flights'
                           render={() => <ListOfFlights/>}/>
                    <Route exact={true} path='/tourists'
                           render={() => <ListOfTourists/>}/>
                    <Route exact={true} path='/tourists/:id' component={Tourist}/>
                    <Route exact={true} path='/flights/:id' component={Flight}/>
                    <Route path='/save-tourist' component={SaveTouristForm}/>
                    <Route path='/save-flight' component={SaveFlightForm}/>
                    <Route exact={true} path={`/tourists/:id/addFlight`} component={AddTouristFlight}/>
                    <Route exact={true} path={`/tourists/:id/deleteFlight`} component={DeleteFlightOfTourist}/>
                </div>
            </Router>
        )
    }
}

export default FlightApplication;