import React from 'react'
import {Link} from 'react-router-dom'

class Flights extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            flightList: [],
        }
    }

    render() {
        return <div className="container-fluid">
            {this.state.flightList.map(flight => {
                return <div
                    key={flight.id}>{flight.id}, {flight.whereFrom}, {flight.whereTo}, {flight.flightDate}, {flight.arrivalDate}, {flight.seatCount} {flight.touristCount}, {flight.ticketPrice}
                    <Link to={`/flights/${flight.id}`}>
                        <div><button className="btn btn-primary">Flight details</button></div>
                    </Link>
                </div>
            })}
        </div>
    }

    componentDidMount() {
        this.findFlights();
    }

    findFlights() {
        fetch("http://localhost:8080/flights")
            .then(response => response.json())
            .then(data => {
                this.setState({flightList: data, isLoading: false});
            })
    }
}

export default Flights;