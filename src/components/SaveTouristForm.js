import React from 'react'
import {Button, Label} from 'react-bootstrap';

class SaveTouristForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            newTouristId: 0,
            name: '',
            surname: '',
            sex: '',
            country: '',
            notes: '',
            birthDate: ''
        }
    }

    render() {
        return <form className="form-group">
            <div>
                <Label htmlFor="name"> Name</Label>
                <div className="col-10">
                    <input className="form-control" type="text" id="name" onChange={(event) =>
                        this.setState({name: event.target.value})
                    }/>
                </div>
            </div>
            <div>
                <Label htmlFor="surname"> Surname</Label>
                <div className="col-10">
                    <input type="text" className="form-control" id="surname" value={this.state.surname}
                           onChange={(event) => {
                               this.setState({surname: event.target.value})
                           }}/>
                </div>
            </div>
            <div>
                <Label htmlFor="sex"> Sex</Label>
                <div className="col-10">
                    <input type="text" className="form-control" id="sex" value={this.state.sex} onChange={(event) => {
                        this.setState({sex: event.target.value})
                    }}/>
                </div>
            </div>
            <div>
                <Label htmlFor="country"> Country</Label>
                <div className="col-10">
                    <input type="text" className="form-control" id="country" value={this.state.country}
                           onChange={(event) => {
                               this.setState({country: event.target.value})
                           }}/>
                </div>
            </div>
            <div>
                <Label htmlFor="notes"> Notes</Label>
                <div className="col-10">
                    <input type="text" className="form-control" id="notes" value={this.state.notes}
                           onChange={(event) => {
                               this.setState({notes: event.target.value})
                           }}/>
                </div>
            </div>
            <div>
                <Label htmlFor="birthDate"> Date of Birth</Label>
                <div className="col-10">
                    <input type="date" className="form-control" id="birthDate" value={this.state.date}
                           onChange={(event) => {
                               this.setState({birthDate: event.target.value})
                           }}/>
                </div>
            </div>
            <Button className="btn btn-primary"onClick={() => {
                this.saveNewFlight(JSON.stringify({
                    name: this.state.name,
                    surname: this.state.surname,
                    sex: this.state.sex,
                    country: this.state.country,
                    notes: this.state.notes,
                    birthDate: this.state.birthDate
                }));
                alert("tourist saved!")
            }}> Save tourist!</Button>
        </form>
    }

    saveNewFlight(stringifiedUser) {
        fetch('http://localhost:8080/tourists', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: stringifiedUser
        })
    }
}

export default SaveTouristForm;